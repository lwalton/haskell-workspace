import Control.Concurrent
import Control.Concurrent.STM
import Data.List
import Text.Printf

data Item = Water | Food deriving (Eq, Ord, Show)

data Player = Player 
     {name :: String
	 , gold :: TVar Int
	 , inventory :: TVar [Item]
	 }

createPlayer n g is = do
     g' <- newTVar g
     is' <- newTVar is
     return $ Player n g' is'
	
main :: IO ()
main = do
     players <- atomically . sequence $
         zipWith3 createPlayer ["Alice", "Bob"] [5,0] [[Water], [Food]]
     let alice = players !! 0
         bob = players !! 1
     printCurrentState players
     
     --atomically $ transferItemSTM Water alice bob
     --printCurrentState players
     
     --atomically $ transferGoldSTM 4 alice bob
     --printCurrentState players
     
     --atomically $ buyItemSTM Water 1 bob alice
     --printCurrentState players
     
     atomically $ orElse (buyItemSTM Food 20 alice bob) (buyItemSTM Food 1 alice bob)
     printCurrentState players
	 
printCurrentState ps = mapM_ printPlayer ps

printPlayer p = do
     g <- atomically $ readTVar (gold p)
     is <- atomically $ readTVar (inventory p)
     printf "%s has %d gold and the following items: %s\n" (name p) g (show is)
	 
transferGoldSTM :: Int -> Player -> Player -> STM ()
transferGoldSTM amount source target = do
     s <- readTVar (gold source)
     t <- readTVar (gold target)
     writeTVar (gold source) (s-amount)
     writeTVar (gold target) (t+amount)


transferItemSTM :: Item -> Player -> Player -> STM ()
transferItemSTM item source target = do
     s <- readTVar (inventory source)
     t <- readTVar (inventory target)
     writeTVar (inventory source) (delete item s)
     writeTVar (inventory target) (item : t)

buyItemSTM :: Item -> Int -> Player -> Player -> STM ()
buyItemSTM item amount buyer seller = do
     bgold <- readTVar (gold buyer)
     sinv <- readTVar (inventory seller)
     if (bgold < amount || not (item `elem` sinv)) then retry 
                       else do
                         transferItemSTM item seller buyer
                         transferGoldSTM amount buyer seller
						 
						 
						 
						 