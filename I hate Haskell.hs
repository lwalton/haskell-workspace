import Control.Concurrent

main :: IO()
main = do 
    forkIO thread 
    return ()

thread :: IO()
thread = do putStrLn  "hi"
            threadDelay(1000000)
            putStrLn "Hi again"