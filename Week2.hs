typeInt ::  Eq a => [a] -> [a] -> Bool
typeInt  ns xs = ns == xs
-- typeInt [8,6] [8,6] response True

typeInt2 ::[Int] -> [Int] -> Int -> Bool
typeInt2 ns xs x = if x <= length xs
	then xs!!x == ns!!x
	else False
--typeInt2 [8,6,7] [7,7,5] 1 response False

second :: [a] -> a
second xs = head(tail xs)
--second [1,2,3,4] response 2

swap :: (a, b) -> (b, a)
swap (x,y) = (y,x)
-- swap (1,2) response (2,1)

first :: (a, b, c) -> a
first (x,y,z) = x
-- first (1,2,3) response 1

add :: Num a => a -> a -> a -> a
add x y z = x + y + z
--add 1 2 3 response 6

ordered :: Ord a => (a,a,a) -> Bool
ordered (x,y,z) = x <= y && y <= z
-- ordered (1,2,3) response True

iszero :: Eq a => Num a => a -> Bool
iszero n = (n == 0)
-- iszero 0 response True

pal :: String -> Bool
pal x  = x == reverse x
--pal "madam" repsonse True

mkpal :: String -> String
mkpal x = if pal x
	then x
	else x ++ reverse x
-- mkpal "Luke" response "LukeekuL"