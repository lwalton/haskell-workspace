doubleMe :: Int -> Int
doubleMe x = x + x 
-- doubleMe(doubleMe(867775)) response 3471100

doubleUs :: Int -> Int -> Int
doubleUs x y = x + x + y + y
--doubleUs 2 4 response 12

doubleUs2 :: Int -> Int -> Int
doubleUs2 x y = doubleMe(x) + doubleMe(y)
--doubleUs2 2 4 response 12

quadrupleMe :: Int -> Int
quadrupleMe x  = doubleMe(doubleMe(x))
--quadrupleMe 867775 response 3471100

last2 :: [Int] -> Int
last2 x = head (reverse x)
--last2 [1..5] response 5

init2 :: [Int] -> [Int]
init2 x = reverse (tail(reverse x))
--init2 [1..5] response [1,2,3,4]

last3 :: [Int] -> Int
last3 x = x!!(length x - 1)
--last3 [1..5] response 5

init3 :: [Int] -> [Int]
init3 x = take (length x-1) x
--init3 [1..5] response [1,2,3,4]