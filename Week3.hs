myNot :: Bool -> Bool
myNot p = if p == False then True else False
--myNot True response False
--myNot False response True

myNot2 :: Bool -> Bool
myNot2 p	| p == True = False
			| otherwise  = True
--myNot True response False
--myNot False response True

safetail1 :: Eq a => [a] -> [a]
safetail1 xs = if xs == [] then [] else tail xs
--safetail1 [] response []

safetail2 :: Eq a => [a] -> [a]
safetail2 xs	| xs == [] = []
				| otherwise = tail xs
--safetail2 [] response []

safetail3 ::  [a] -> [a]
safetail3 [] = []
safetail3 xs = tail xs
--safetail3 [] response []

(|||) :: Bool -> Bool -> Bool
(|||) p q = if p == False && q == False then False else True
-- (|||) False False response False



--(|||) True _ = True
--(|||) _ True = True
--(|||) False False = False


test1 = [x^2 | x <- [1.. 100000], x^2 < 100000] 

test2 = [x | x <- [100..200], x `mod` 7 /= 0 ]

test3 = [x | x <- [100..200], x `mod` 7 /= 0 && ((show x) == reverse (show x)) ]