factorial :: Eq a => Num a => a -> a
factorial 0 = 1
factorial x = x * factorial(x - 1)
--factorial 40000 This can work out around factorial 40000 in around 4s

rev1::[a]->[a]
rev1 [] 	= []
rev1 (x:xs)	= rev1 xs ++[x]
--rev1 [1..90000] response counts backwards from 90000


rev2::[a]->[a]
rev2 xs = rev2 [] xs where 
	rev2 :: [a]->[a]->[a]
	rev2 acc[] = acc
	rev2 acc (x:xs) = rev2 (x:acc) xs
--rev2 [1..90000] response counts backwards from 90000

insertionSort :: Ord a => [a] -> [a]
insertElement :: Ord a => a -> [a] -> [a]
insertElement x [] = x:[]
insertElement x (y:ys) = if (x < y) then x:y:ys else y : insertElement x ys
insertionSort [] = []
insertionSort (x: xs) = insertElement x (insertionSort(xs))
--insertionSort [4,5,3,1,2] response [1,2,3,4,5]

triples :: Int -> [(Int,Int,Int)]
triples n= [(x,y,z)| x <- [1..n], y <-[1..n], z<-[1..n], x^2 + y^2 == z^2]
--triples 8 response [(3,4,5), (4,3,5)]