import Test.QuickCheck

--Group 33 Stuart James Trevor Kenward 870278, Fynnian Nelder-Hunt 828438, Luke Walton 867775


--Question 1)a.---------------------------------------------------------------------------

threeAverage :: Float -> Float -> Float -> Float
threeAverage x y z = (x + y + z) / 3
--threeAverage 867775 870278 828438 response: 855497.0

howManyBelowAverage1 :: Float -> Float -> Float -> Int
howManyBelowAverage1 x y z = length (filter (< avg) [x , y , z])
                             where avg = threeAverage x y z
--howManyBelowAverage1 867775 870278 828438 response: 1						

howManyBelowAverage2 :: Float -> Float -> Float -> Int
howManyBelowAverage2 x y z = length[a | a <-[x,y,z], a < avg]
                             where avg = threeAverage x y z
--howManyBelowAverage2 867775 870278 828438 response: 1

--Question 1)b.---------------------------------------------------------------------------

checkCorrectness :: Float -> Float -> Float -> Bool
checkCorrectness x y z = howManyBelowAverage1 x y z == howManyBelowAverage2 x y z
--checkCorrectness 867775 870278 828438 response True

--Question 2)-----------------------------------------------------------------------------

pizzaPrice :: Float -> Float -> Float
pizzaPrice x y =  (0.001* (pi * (x/2)^2) + ((pi * (x/2)^2)*0.002* y))*1.6
--pizzaPrice 32 2 response: 6.4339824

--This function calls pizzaPrice and compares the prices of the 2 pizzas to work out whether the Bambini or Famiglia is more expensive.
comparePizza :: String
comparePizza = if (pizzaPrice 14 6) > (pizzaPrice 32 2)
               then "Bambini"
               else "Pizza Famiglia"
--comparePizza response "Pizza Famiglia"

--Question 3)-----------------------------------------------------------------------------

divides :: Integer -> Integer -> Bool
divides x y = y `mod` x == 0

prime :: Integer -> Bool
prime n = n > 1 && and [not(divides x n) | x <- [2..(n-1)]]

allPrimes :: [Integer]
allPrimes = [ x | x <- [2..], prime x]

isPrimeTwin :: Integer -> Bool
isPrimeTwin x = prime x && prime (x+2)

allPrimesBetween :: Integer -> Integer -> [Integer]
allPrimesBetween x y = [a | a <- [x..y], prime a]
--allPrimesBetween 1 8 response [2,3,5,7]

primeTest :: [Bool]
primeTest = map prime [2..]
--primeTest response displays infite list containing True and False correctly

primeTest2 :: [(Integer, Bool)]
primeTest2 = [(a,prime a) | a <- [2..]]
--primeTest response displays infite list containing True and False  and the number correctly

primeTwins :: Integer -> Int
primeTwins x = length (filter (isPrimeTwin) [2..x])
--primeTwins 1000 response 35

--Question 4)-----------------------------------------------------------------------------

--This returns a list of Ints which is the length of each initial list.
listLength :: [[Int]] -> [Int]
listLength xs = map length xs


-- --We have made 2 solutions for q4 part 2. 
-- --The first returns a list of a list of ints as you cant returna a null Int value
-- lessThan :: [Int] ->  [Int]
-- lessThan xs = 
    -- if (xs == []) then [] else
        -- if ((all ( == minimum xs) xs) && (length xs > 1)) 
        -- then []
        -- else [minimum xs]

-- smallestElement :: [[Int]] ->  [[Int]]
-- smallestElement xs = map lessThan xs
-- --smallestElement [[1,1],[1],[1,2,3]] response [[],[1],[1]]

--This solution isn't perfect as, although it only returns a list of Ints but returns Just before any Int variable and nothing if there isnt a minimum value.
lessThan :: [Int] -> Maybe Int
lessThan xs = 
	if (xs == []) then Nothing else
        if ((all (== minimum xs) xs) && (length xs > 1))
        then Nothing
        else Just (minimum xs)

smallestElement :: [[Int]] -> [Maybe Int]
smallestElement xs = map lessThan xs
--smallestElement [[1,1],[1],[1,2,3]] response [Nothing, Just 1, Just 1]

--Question 5) -----------------------------------------------------------------------------

expmod :: Integer -> Integer -> Integer -> Integer
expmod m e n = if e <= 1 then (m `mod` n) -- Base case
               else (m * (expmod m (e-1) n) `mod` n)
-- expmod 5 828438 10 response 5
			   
expmodfast :: Integer -> Integer -> Integer -> Integer
expmodfast m e n
       | e <= 1 = m `mod` n -- Base case
       | e `mod` 2 == 1 = (m * ((expmodfast m (quot (e-1) 2) n)^2)) `mod` n --Checks if e is odd, if so then e-1 is even and can be halved.
	   | otherwise = (m * (expmodfast m (e-1) n)) `mod` n -- If e is even then e-1 is odd therefore old method is used.
-- expmodfast 5 828438 10 response 5