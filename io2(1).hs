-- IO examples:

-- 1) printing to the console:


-- putStr/putStrLn prints a string;
-- print prints an arbitrary type.

-----------------------------------------------------------------
-- 2) do notation: allows to do several things 
-- use either indentation or {}brackets with ;

test = do putStr "2 + 2 = "
          print (2+2)

test2 = do {putStr "2 + 2 = "; print (2+2)}

--------------------------------------------------      
-- 3) Read from keyboard



main :: IO ()
main = do putStrLn "What is 2 + 2"
          x <- getLine 
          if (read x) == 4
	  then putStrLn "You are right!"
	  else putStrLn "I am sorry!"






-- getLine and read can be joined into one function: ReadLn


main2 :: Int -> IO()
main2 n = do putStr "What is 2 + "
             print n
             x<-readLn
             if (x == 2+n) then putStrLn "Excellent!"
                           else putStrLn "I am sorry!"       





-- Here an example for a subroutine that does some of the work
-- and feeds a result back to main.
-- 1) it asks for a result,
-- 2) transfers the result into an Int and
-- 3) returns it to the function that called it.
-- note its type is not Int but IO(Int)

myread :: IO(Int)
myread  = do putStr "Please enter the result: "
             x<-readLn
             return x

main5 :: IO ()
main5 = do putStrLn "What is 2 + 2"
           result <- myread   
           if result == 4 then putStrLn "You are right!"
                          else putStrLn "I am sorry!"




----------------------------------------------------------------

a:: IO (String,String)
a = do x <- getLine
       y <- getLine
       return (x,y)


a2:: IO (Char,Char)
a2 = do x <- getChar
        --- getChar -- eats newline char
        y <- getChar
        return (x,y)





