part1 :: IO()
part1 = do putStrLn "Enter a large Number: "
           x <- getLine
           putStrLn x
		   
		   
part2 :: IO()
part2 = do putStrLn "Enter a large Number: "
           x <- getLine
           if (read x) < 1000 
           then putStrLn "That's not large"
           else if (read x) < 10000 then putStrLn "That's a slightly large number"
           else putStrLn "Well done - that's a large number"
		   
part3 :: IO()
part3 = do putStrLn "Enter a large Number: "
           x <- getLine
           if (read x) < 1000 
           then part3
           else if (read x) < 10000 then putStrLn "That's a slightly large number"
           else putStrLn "Well done - that's a large number"

--part4		   
square:: Integer -> Integer
square x = x*x

square3 :: Integer -> Integer
square3 x = (x*x)+3

palindrome :: String -> Bool
palindrome x  = x == reverse x

testlist =["madam", "adam", "otto", "else", "kajak", "seas"]


test1 = map square3 [1..500]
--test1 = [(square x) + 3|x<-[1..500]]

test2 = [palindrome x| x <- testlist]
--test2 = map palindrome testlist

test3 = filter palindrome testlist 
--test3 = [x|x <-testlist, palindrome x]